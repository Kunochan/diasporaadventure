GM's Section
============

Everything in this folder is information that should only be available to game masters. Players may read this after completing the module, but may have some of their fun spoiled, and instead should consult the [player section](../forPlayers/README.md).
