Orion Space Battleships
=======================

Stat blocks for ships the players will encounter in Throne 94

Orion space Battleship
----------------------

These ships will be patrolling the space in Throne 94 and will attempt to prevent the players from accomplishing their mission as if their lives depend on it. They will ignore most attempts at diplomacy and will respond to any percieved aggression with neuclear warheads.

|  Tech 1 | 11pts | **Stunts** | **Aspects** |
|-------------|--------|--------|---------|
| **V-shift :** | 3 | Extended Magazines  | *Free aspect* |
| **Trade :** | 0 | Vector Randomizer  | *Free aspect* |
| **Beam :** | 1  | Firewall | *Free aspect* |
| **Torpedo :** | 2 |   | *Free aspect* |
| **EW :** | 0 |   | *Free aspect* |
| **Hull :** | **[ ][ ][ ][ ]** | | |
| **Data :** | **[ ][ ][ ]** | | |
| **Heat :** | **[ ]** | | |

Factory Cathedral
-----------------

This asteroid base is the headquarters of the priesthood where they manufacture and repair their warships. Threatening it will cause them to split their forces between the planet and their cathedral.

|  Tech 1 | 11pts | **Stunts** | **Aspects** |
|-------------|--------|--------|---------|
| **V-shift :** | 0 |   | *Free aspect* |
| **Trade :** | 3 |   | *Free aspect* |
| **Beam :** | 0  | Firewall | *Free aspect* |
| **Torpedo :** | 0 |   | *Free aspect* |
| **EW :** | 0 |   | *Free aspect* |
| **Hull :** | **[ ][ ][ ][ ]** | | |
| **Data :** | **[ ][ ][ ]** | | |
| **Heat :** | **[ ]** | | |

Logistics Orion
---------------

A less armed and armored version of the Orion space battleship used for logistics work. Attacking it would disrupt the Priesthood's operations.

|  Tech 1 | 11pts | **Stunts** | **Aspects** |
|-------------|--------|--------|---------|
| **V-shift :** | 3 | Extended Magazines  | *Free aspect* |
| **Trade :** | 0 | Vector Randomizer  | *Free aspect* |
| **Beam :** | 1  | Firewall | *Free aspect* |
| **Torpedo :** | 2 |   | *Free aspect* |
| **EW :** | 0 |   | *Free aspect* |
| **Hull :** | **[ ][ ][ ][ ]** | | |
| **Data :** | **[ ][ ][ ]** | | |
| **Heat :** | **[ ]** | | |
