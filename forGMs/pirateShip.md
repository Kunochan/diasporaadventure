Pirate ships
============

Stat blocks for ships the players will use or encounter in Pirate Cove.

Private(er) starship
--------------------
The ship the players will most likely use in the course of the adventure, and which will serve as their reward for completing it. The players should have a degree of freedom to customize this ship by assigning aspects or even tweaking stats, but otherwise this is the basic stat block they will be given.

|  Tech 2 | 17pts   | **Stunts**        | **Aspects**                  |
|---------------|---|-------------------|------------------------------|
| **V-shift :** | 3 | T2 Slipstream     | *Free as in Lunch*           |
| **Beam :**    | 2 | Skeleton Crew     | *Blockade runner*            |
| **Torpedo :** | 0 | Interface Vehicle | *30 day trial*               |
| **EW :**      | 2 | Civilian          | *Twin drive*                 |
| **Trade:**    | 2 | Vector randomizer | *Free aspect*                |
| **Hull :**    | **[ ][ ][ ]** |       |                              |
| **Data :**    | **[ ][ ][ ]** |       |                              |
| **Heat :**    | **[ ][ ][ ]** |       |                              |

*[Free as in Lunch]: There is no such thing as a free lunch! If you think you are getting something for free, you're probably on the menu! [+] when freedom is inherently good, [-] when free implies a lack of value.

*[Penetration Testing]: The players are being hired as "Penetration testers" testing the orbital defenses of a neighboring planet by delivering a TAV to the surface. [+] when bypassing or otherwise penetrating defenses. [-] when people are bothered by how aggressive this sounds.

*[30 day trial]: This spacecraft is being distributed as shareware. The players have 30 days to disable it's auto-destruct protocols by either completing the mission, cracking the registration key, or paying cash, before it explodes. [+] when this gives a much needed sense of urgency, [-] when it just makes things stressful!

*[Black ship, White Payload]: The spacecraft is painted a very aggressive looking black, but the shuttle it carries is pristine white. These colors imply different things to different cultures [+] when they imply good things. [-] when they imply bad things.

*[Free aspect]: The players are free to make up the ship's last aspect, and will be able to change the other aspects after the adventure is over.

Pirate(d) starship
-------------------
If any of the players have the military grade piloting stunt, they may chose this ship instead. This ship is created from stolen templates and is up to military standards. Using it in some systems may attract unwanted attention. It also requires a crew who may or may not be trustworthy. Alternately, this statblock can be used for a generic pirate ship with a few modifications. See the Variants section at the end of this document.

|  Tech 2 | 17pts   | **Stunts**        | **Aspects**                  |
|---------------|---|-------------------|------------------------------|
| **V-shift :** | 3 | T2 Slipstream     | *30 day trial*               |
| **Beam :**    | 2 | Interface Vehicle | *Ragtag crew*                |
| **Torpedo :** | 2 | Vector randomizer | *Hunter missiles /no ammo*   |
| **EW :**      | 3 |                   | *Twin drive*                 |
| **Trade:**    | 2 |                   | *Free aspect*                |
| **Hull :**    | **[ ][ ][ ]** |       |                              |
| **Data :**    | **[ ][ ][ ]** |       |                              |
| **Heat :**    | **[ ][ ][ ]** |       |                              |

The Bazaar
----------
This is where the players start out their adventure. It is a starport rather than a ship itself, but can participate in starship combat so thus has a statblock. If the players attack or otherwise cause trouble for the Bazaar, two or more Security ships (see Variants section) will suddenly appear to defend it.

|  Tech 2  | 17pts  | **Stunts**      | **Aspects**                    |
|---------------|---|-----------------|--------------------------------|
| **V-shift :** | 0 | Self sufficient | *Bazaar of the Bizarre*        |
| **Beam :**    | 0 | Point Defense   | *Any port*                     |
| **Torpedo :** | 0 |                 | *Pirate networks*              |
| **EW :**      | 4 |                 | *3d printed starships*         |
| **Trade:**    | 4 |                 | *'Take it outside'*            |
| **Hull :**    | **[ ][ ][ ][ ]**    |                                |
| **Data :**    | **[ ][ ][ ][ ]**    |                                |
| **Heat :**    | **[ ]**             |                                |

Variants
========
Guidelines for creating new ships based on the above stat-blocks.

Pirates!
--------

If the players spend too much time in Pirate Cove looking vulnerable, they will be attacked by a ship with the Pirate(d) ship statblock and the following aspects

1. *Free as in freebooter*
2. *Pirate(d) ship*
3. *Ragtag crew*
4. *Hunter missiles / out of ammo*
5. *Twin drive*

*[Hunter missiles]: This ship is armed with long range smart missiles capable of operating as semi autonomous drones and returning to the ship if not used offensively. [+] when finding creative uses for these drones [-] when they need to be replaced.

*[out of ammo]: This aspect may be compelled to prevent an attacker from making torpedo attacks.

Easy prey
---------

These stats represent civilian shipping in the system. They may or may not be escorted by the private security ships listed below.
Take the Private(er) statblock with the *interface vehicle* and *vector randomizer* stunts replaced with another level of trade, and the following aspects.

1. 
2. 
3. 
4. *Modular Cargo hauler*
5. *Torch drive*

*[Modular Cargo hauler]: This ship has been modified to haul standardized hexagonal cargo containers. [+] when this improves economics [-] when this makes it hard for it to defend itself.

*[Torch drive]: This ship's twin drives have been replaced with a much more efficient single drive configuration. [+] when economy matters [-] when the lack of mobility and reliability are more important.

Private Security
----------------

If the players cause trouble too close to the Port, they will be attacked by two ships with the Pirate(d) ship statblock with the following modifications. Swap the *Interface Vehicle* stunt with *Extended magazines* and use the following aspects,

1. *Troubleshooters*
2. *'Have you tried turning your life support on and off again?'*
3. *Pirate(d) ship*
4. *Cluster missiles*
5. *Twin drive*

*[Troubleshooters]: The crew of this ship are highly skilled but minimally disciplined. Their job is to find trouble and shoot it until it goes away. [+] when solving problems violently. [-] when they have to be restrained.

*['Have you tried turning your life support on and off again?']: This ship has an impressive, if potentially ramshackle electronic warfare array. [+] when doing anything unusual with computers [-] when inexplicable bugs pop up.

*[Pirate(d) ship]: This spacecraft is built off military templates stolen from a long extinct space-faring civilization. While the original designers are unlikely to sue for copyright violation, most currently existing governments are uncomfortable with the idea of military grade technology being freely distributed. [+] when a well known design makes maintenance easy. [-] when authorities disapprove.

*[Cluster missiles]: The primary weapon is a long range bus missile that deploys multiple warheads at close range to saturate point defenses. [+] when overwhelming defenses. [-] when the bus can be shot down before deploying it's payload.

*[Twin drive]: Propulsion, power, and attitude control is provided by a pair of magnetically vectored fusion torch drives. [+] when this configuration provides high maneuverability. [-] when this configuration is less efficient than a single drive.
