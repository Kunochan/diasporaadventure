The Bazaar
==========

Pirate cove is a solar system with no planets, just a protoplanetary disk composed of gas, dust, and chunks of rock. While it lacks planets, the disk contains plenty of resources and even more places to hide. Over the millennia, countless civilizations have attempted to establish mining operations in the dust clouds, only to fall victim to pirates lurking in the same clouds. 

The Civilizations that have been able to survive in such an environment were the ones who were best able to make themselves indispensable to the pirates. Those who could provide shelter from the storms and a place to trade their ill gotten gains. Over thousands of (local, solar) years, these pirate ports have evolved in a hostile environment into some of the most advanced civilizations in the cluster. A ring of self replicating machines that live in the dust clouds and provide port services to any spacecraft that visits them regardless of affiliation.

While the ring appears to be a solid object from a distance, it is in fact a swarm of smaller objects. Thousands of distinct cultures of self replicating spacecraft using millions of distinct taxonomies of deralict spacecraf.
