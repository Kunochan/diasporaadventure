A job for the Fractal
=====================

This is notes for a Diaspora scenario I intend to run to teach myself and my DnD group how to play and run Diaspora. It may eventually be published as part of [Akashic Repo](https://akasharepo.wordpress.com/), or printed through one of those online PDF retailers, but for now it will take the form of a bunch of markdown files in this git repository.

The [forPlayers/](/forPlayers/README.md) directory contains information that should be provided to players. the [forGMs/](/forGMs/README.md) directory contains information and instructions for the game master. More information about the Diaspora role playing game can be found on the [VSCA website](http://www.vsca.ca/Diaspora/) along with a complete [SRD](http://www.vsca.ca/Diaspora/diaspora-srd.html). This module is avalable under the [OGL](/LISCENCE).
